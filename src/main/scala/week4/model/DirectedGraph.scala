package week4.model

import scala.io.Source

/**
  * Created by Bertrand on 2/10/2016.
  */
object DirectedGraph {

  def apply(size: Int): DirectedGraph = {
    new DirectedGraph(size)
  }

  def fromFile(fileName: String) = {
    val source = Source.fromFile(fileName).getLines()
    val size = source.next().toInt
    val graph = new DirectedGraph(size)
    while (source.hasNext) {
      source.next().split(" ") match {
        case Array(from, to) => graph.addEdge(from.toInt - 1, to.toInt - 1)
      }
    }
    graph
  }
}

class DirectedGraph(val size: Int) {

  private var nbEdges = 0

  def NbEdges = nbEdges
  def Edges = adj

  val adj = Array.fill(size) {
    List.empty[Int]
  }

  def addEdge(from: Int, to: Int): Unit = {
    adj(from) = to :: adj(from)
    nbEdges = nbEdges + 1
  }

  def reversed: DirectedGraph = {
    val reversedGraph = DirectedGraph(size)
    for (from <- 0 until size; to <- adj(from)) {
      reversedGraph.addEdge(to, from)
    }
    reversedGraph
  }

  override def toString: String = adj.toSeq.toString

  def canEqual(other: Any): Boolean = other.isInstanceOf[DirectedGraph]

  override def equals(other: Any): Boolean = other match {
    case that: DirectedGraph =>
      (that canEqual this) &&
        adj.deep == that.adj.deep &&
        size == that.size
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(adj, size)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
