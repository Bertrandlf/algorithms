package week1.model

/**
  * Created by Bertrand on 25/09/2016.
  */
class ListInversions(val list: List[Int], val inversions: Long) {

  def canEqual(other: Any): Boolean = other.isInstanceOf[ListInversions]

  override def toString() = s"${this.list} \n ${this.inversions}"

  override def equals(other: Any): Boolean = other match {
    case that: ListInversions =>
      (that canEqual this) &&
        list == that.list &&
        inversions == that.inversions
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(list, inversions)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
