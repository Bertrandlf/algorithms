package week2

import week2.model.QuickSortList

/**
  * Created by Bertrand on 14/09/2016.
  */
object QuickSort {

  type valueIndex = (Int, Int)

  def quickSort(l: QuickSortList, leftIndex: Int, rightIndex: Int,
                partition: (QuickSortList, Int, Int) => Int = partitionMedian): Unit = {
    val li = leftIndex
    val ri = rightIndex
    if (li < ri) {
      val newPivotIndex = partition(l, li, ri)
      quickSort(l, li, newPivotIndex - 1, partition)
      quickSort(l, newPivotIndex + 1, ri, partition)
    }
  }

  // partitions and returns the pivot index, using first element as pivot
  def partitionFirst(qsl: QuickSortList, leftIndex: Int, rightIndex: Int): Int = {
    // by default, pivot index will always be most left value
    val pivot = qsl.list(leftIndex)
    partition(qsl, leftIndex, rightIndex, pivot)
  }

  // partitions and returns the pivot index, using last element as pivot
  def partitionLast(qsl: QuickSortList, leftIndex: Int, rightIndex: Int): Int = {
    // by default, pivot index will always be most right value
    val pivot = qsl.list(rightIndex)
    qsl.swap(leftIndex, rightIndex)
    partition(qsl, leftIndex, rightIndex, pivot)
  }

  // partitions and returns the pivot index, using best of three elements: first, middle, last
  def partitionMedian(qsl: QuickSortList, leftIndex: Int, rightIndex: Int): Int = {
    val l = qsl.list
    val middleIndex = (leftIndex + rightIndex) / 2
    // by default, pivot index will be best of three
    val left = (l(leftIndex), leftIndex)
    val right = (l(rightIndex), rightIndex)
    val middle = (l(middleIndex), middleIndex)
    val pivotIndex = getMedianIndex(left, middle, right)

    qsl.swap(leftIndex, pivotIndex)
    val pivot = l(leftIndex)
    partition(qsl, leftIndex, rightIndex, pivot)
  }

  def partition(qsl: QuickSortList, leftIndex: Int, rightIndex: Int, pivot: Int): Int = {
    qsl.comparisons += rightIndex - leftIndex
    var i, j = leftIndex + 1
    val l = qsl.list
    for (j <- i until l.length) {
      if (l(j) < pivot) {
        qsl.swap(j, i)
        i += 1
      }
    }
    val newPivotIndex = i - 1
    qsl.swap(newPivotIndex, leftIndex)
    newPivotIndex
  }

  def getMedianIndex(avi: valueIndex, bvi: valueIndex, cvi: valueIndex): Int = {
    val a = avi._1
    val b = bvi._1
    val c = cvi._1

    if ((a - b) * (c - a) >= 0)
      return avi._2
    if ((b - a) * (c - b) >= 0)
      return bvi._2
    cvi._2
  }

}
