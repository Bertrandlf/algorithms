package week2.model

/**
  * Created by Bertrand on 25/09/2016.
  */
class QuickSortList(val list: Array[Int], var comparisons: Int = 0) {

  override def toString = s"${this.list.toList} \nNumber of comparisons: ${this.comparisons}"

  def length = list.length

  def maxIndex = length - 1

  def swap(i: Int, j: Int) = {
    val t = list(i)
    list(i) = list(j)
    list(j) = t
  }
}
