package week2

import benchmark.Performance.timeIt
import week2.model.QuickSortList

import scala.io.Source

object QuickSortAssignment {

  def getInput: Array[Int] = Source.fromFile("src\\resources\\week2_IntegerArray.txt").getLines.toArray.map(_.toInt)

  def main(args: Array[String]): Unit = {
    val qslFirst = new QuickSortList(getInput)
    val qslLast = new QuickSortList(getInput)
    val qslMedian = new QuickSortList(getInput)

    val timeFirst = timeIt(QuickSort.quickSort(qslFirst, 0, qslFirst.maxIndex, QuickSort.partitionFirst))._1
    println(s"Comparisons pivot FIRST element: ${qslFirst.comparisons}\n$timeFirst")

    val timeLast = timeIt(QuickSort.quickSort(qslLast, 0, qslLast.maxIndex, QuickSort.partitionLast))._1
    println(s"Comparisons pivot LAST element: ${qslLast.comparisons}\n$timeLast")

    val timeMedian = timeIt(QuickSort.quickSort(qslMedian, 0, qslMedian.maxIndex, QuickSort.partitionMedian))._1
    println(s"Comparisons pivot MEDIAN element: ${qslMedian.comparisons}\n$timeMedian")
  }
}