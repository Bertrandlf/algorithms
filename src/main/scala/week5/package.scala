import scala.collection.mutable

/**
  * Created by Bertrand on 7/10/2016.
  */
package object week5 {
  type NodePath = (Int, Double)
  type NodeEdges = mutable.PriorityQueue[NodePath]
}
