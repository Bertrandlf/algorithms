package week5

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Created by Bertrand on 19/10/2016.
  */

case class Edge(to: Int, weight: Long)

class WeightedGraph(val n: Int) {

  private val edges = Array.fill(n) {
    List.empty[Edge]
  }
  var m = 0

  def NbEdges = m

  def addEdge(from: Int, to: Int, weight: Long) = {
    edges(from) = Edge(to, weight) :: edges(from)
    m += 1
    this
  }

  override def toString: String = edges.toList.toString

  def distances(from: Int) = {
    val heap = mutable.PriorityQueue.empty(MinOrder)
    val maxDist = 10000L
    val distances = Array.fill(n)(maxDist)
    val visited = ListBuffer(from)

    heap.enqueue((from, 0L))
    distances(from) = 0L
    while (visited.length != n) {
      heap.minBy(_._2) match {
        case (node, weight) =>
          var minVisited = -1
          var minDistance = maxDist
          var newDist = maxDist
          edges(node).filter(edge => !visited.contains(edge.to)).foreach(fringeEdge => {
            heap.enqueue((fringeEdge.to, fringeEdge.weight + distances(node)))
            newDist = distances(node) + fringeEdge.weight
            //if we found a better path, update
            if (distances(fringeEdge.to) > newDist) {
              distances.update(fringeEdge.to, newDist)
            }
            //we keep track of the smallest step
            if (minDistance > newDist) {
              minVisited = fringeEdge.to
              minDistance = newDist
            }
          })
          //effectively process only node with shortest distance
          visited += minVisited
      }
      heap.dequeue
    }
    distances
  }
}

object WeightedGraph {
  def apply(n: Int) = new WeightedGraph(n)

  def fromFile(fileName: String) = {
    val lines = Source.fromFile(fileName).getLines()
    val weightedGraph = apply(lines.next().toInt)
    lines.foreach(line => {
      line.split("\\s+") match {
        case Array(n, edges@_*) =>
          edges foreach (edge => edge.split(",") match {
            case Array(to, weight) => weightedGraph.addEdge(n.toInt - 1, to.toInt - 1, weight.toLong)
          })
      }
    })
    weightedGraph
  }
}
