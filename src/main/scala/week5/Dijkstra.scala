package week5

import benchmark.Performance

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.{BufferedSource, Source}

/**
  * Created by Bertrand on 7/10/2016.
  */
object Dijkstra {

  val MAX_DISTANCE = 1000000D

  def main(args: Array[String]): Unit = {
    val assignmentFile: String = "src/resources/week5/week5_dijkstraData.txt"
    val assignmentFileWeighted: String = "src/resources/week5/week5_weightedGraph.txt"

    val distances = Performance.timeIt(computeDistance(assignmentFile, 1))
    println(s"Dijkstra ugly ${distances._1}")
    println(List(7, 37, 59, 82, 99, 115, 133, 165, 188, 197).map(key => distances._2(key).toInt).mkString(","))
    println(distances._2.toSeq.sortBy(_._1).toList.map(_._2.toInt))
    val dataStructureDistance = Performance.timeIt(computeDistanceWeightedGraph(assignmentFileWeighted, 1))
    println(s"Dijkstra pretty, but incorrect ${dataStructureDistance._1}")
    println(List(7, 37, 59, 82, 99, 115, 133, 165, 188, 197).map(key => dataStructureDistance._2(key-1).toInt).mkString(","))
    println(dataStructureDistance._2.toList)
  }

  def computeDistanceWeightedGraph(fileName: String, starNode: Int): Array[Long] = {
    WeightedGraph.fromFile(fileName).distances(starNode - 1)
  }

  def computeDistance(fileName: String, starNode: Int): Map[Int, Double] = {
    val source = Source.fromFile(fileName)
    val explored = new ListBuffer[Int]()
    val allNodeEdges: List[NodeEdges] = nodeEdgesFromSource(source)
    var distances: Map[Int, Double] = Map(1 -> 0D)
    //not returned, but useful to debug
    var paths: Map[Int, String] = Map(1 -> "1")

    explored.append(starNode)
    while (explored.size < allNodeEdges.size) {
      var minDistance = MAX_DISTANCE
      var nextEdge = None: Option[(Int, Int, Double)]
      explored.foreach(currentNode => {
        val currentHeap = allNodeEdges(currentNode - 1).filter(nodeEdge => !explored.contains(nodeEdge._1))
        if (currentHeap.nonEmpty) {
          currentHeap.minBy(_._2) match {
            case (node, distance) =>
              val currentDistance: Double = distance + distances(currentNode)
              if (currentDistance < minDistance) {
                nextEdge = Option(currentNode, node, distance)
                minDistance = currentDistance
              }
          }
        }
      })
      //TODO: fix this, using an appropriate data structure like a weighted directed graph
      nextEdge.map(crotte => {
        val nodeFrom = crotte._1
        val nodeTo = crotte._2
        val distance = crotte._3
        val currentHeap = allNodeEdges(nodeFrom - 1)
        distances += (nodeTo -> (distance + distances(nodeFrom)))
        paths += (nodeTo -> (nodeTo + paths(nodeFrom)))
        explored.append(nodeTo)
        currentHeap.dequeue()
      })
    }
    distances
  }

  //an entry in the list for each line
  //each entry in the list is a heap (do we need a heap for that?)
  def nodeEdgesFromSource(source: BufferedSource): List[NodeEdges] =
  source.getLines().foldLeft(List[NodeEdges]()) { (acc, line) =>
    val nodeAndEdges = line.trim.split("\\s+")
    //first element in the line is the node
    buildHeap(nodeAndEdges.drop(1)) :: acc
  }.reverse

  def buildHeap(nodeInfo: Array[String]): NodeEdges =
    nodeInfo.foldLeft(mutable.PriorityQueue.empty(MinOrder)) { (heap, edge) =>
      val path = edge.split(",")
      heap.enqueue((path(0).toInt, path(1).toDouble))
      heap
    }
}
