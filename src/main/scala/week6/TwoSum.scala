package week6

import benchmark.Performance

import scala.collection.immutable.Range.Inclusive
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Created by Bertrand on 13/10/2016.
  */
object TwoSum {

  val inputFile = "src/resources/week6/week6-2sum.txt"

  def main(args: Array[String]) = {
    val data = Performance.timeIt(fillDataStructures(inputFile))
    println(s"Filling data from file ${data._1}")
    //TODO sort array, binary search instead of map
    val range = -10000 to 10000
    val hashTable = data._2._1
    val inputList = data._2._2

    val result = Performance.timeIt(findTwoSums(hashTable, inputList.toArray.sorted, range))
    println(s"Calculating 2 sums %${result._1}")
    //result found is 427
    println(s"Number of two sums : ${result._2}")
  }

  def findTwoSumsBinarySearch(sortedInputList: Array[Double], range: Inclusive): Int = {
    var nbSums = 0
    for (t <- range) {
      var found = false
      var i = 0
      while (!found && i < sortedInputList.length) {
        val x = sortedInputList(i)
        val y = t - x
        if (y != x && binaryFind(y, sortedInputList)) { found = true }
        else { i += 1 }
      }
      if (found) {
        nbSums += 1
        println(s"sum found for $t")
      }
    }
    nbSums
  }

  def binaryFind(toFind: Double, list: Array[Double]): Boolean = list match {
    case Array(value) => toFind == value
    case arrayList =>
      val searchIndex = arrayList.length / 2
      val comparing = arrayList(searchIndex)
      if (toFind == comparing) {
        true
      } else if (toFind > comparing) {
        binaryFind(toFind, arrayList.drop(searchIndex))
      } else {
        binaryFind(toFind, arrayList.dropRight(searchIndex))
      }
  }

  def findTwoSums(hashTable: mutable.HashSet[Double], inputList: Array[Double], range: Inclusive): Int = {
    var nbSums = 0
    for (t <- range) {
      var found = false
      var i = 0
      while (!found && i < inputList.length) {
        val x = inputList(i)
        val y = t - x
        if (hashTable.contains(y) && y != x) { found = true }
        else { i += 1 }
      }
      if (found) {
        nbSums += 1
        println(s"sum found for $t")
      }
    }
    nbSums
  }

  def fillDataStructures(inputFile: String): (mutable.HashSet[Double], ListBuffer[Double]) = {
    val input = Source.fromFile(inputFile)
    val hashTable = mutable.HashSet.empty[Double]
    val inputList: ListBuffer[Double] = new ListBuffer[Double]
    input.getLines().foreach(value => {
      val element = value.toDouble
      if (!hashTable.contains(element)) {
        hashTable.add(element)
        inputList += element
      }
    })
    (hashTable, inputList)
  }
}
