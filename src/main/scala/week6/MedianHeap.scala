package week6

import scala.collection.mutable

/**
  * Created by Bertrand on 14/10/2016.
  */
class MedianHeap {

  private val HLow = new mutable.PriorityQueue[Int]()
  private val HHigh = mutable.PriorityQueue.empty(Ordering.Int.reverse)
  private var medianSum = 0

  def median = HLow.max

  def getMedianSum: Int = medianSum

  def enqueue(x: Int): MedianHeap = {
    if (HLow.isEmpty) {
      HLow += x
    } else {
      //make sure sizes match or HLow.size > HHigh.size
      if (HLow.size == HHigh.size) {
        if (x <= median) {
          HLow.enqueue(x)
        } else {
          HHigh.enqueue(x)
          HLow.enqueue(HHigh.dequeue())
        }
      } else {
        if (x <= median) {
          HLow.enqueue(x)
          HHigh.enqueue(HLow.dequeue())
        } else {
          HHigh.enqueue(x)
        }
      }
    }
    medianSum += median
    this
  }
}
