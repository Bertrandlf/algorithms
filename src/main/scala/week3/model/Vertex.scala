package week3.model

/**
  * Created by Bertrand on 24/09/2016.
  */
class Vertex(val value: String, val merged: String) {
  def this(value: String) = this(value, value)

  def absorb(v: Vertex): Vertex = new Vertex(this.value, this.merged + ", " + v.merged)

  def canEqual(other: Any): Boolean = other.isInstanceOf[Vertex]

  //I consider vertices equal if they have the same value only, we don't care about merged
  //WORST IDEA EVER, GOT THE JOB DONE, BUT NEVER AGAIN
  override def equals(other: Any): Boolean = other match {
    case that: Vertex =>
      (that canEqual this) &&
        value == that.value
    case _ => false
  }

  override def hashCode(): Int = Seq(value).map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)

  override def toString = s"Vertex($merged)"
}