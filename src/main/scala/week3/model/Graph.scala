package week3.model

import scala.util.Random

/**
  * Created by Bertrand on 25/09/2016.
  */

class Graph(vertices: Vertices, edges: Edges) {
  def size = vertices.size

  def nbEdges = edges.size

  def randomEdge(): Edge = edges(Random.nextInt(edges.size))

  def getVertex(key: String): Vertex = vertices.getOrElse(key, new Vertex(key))

  def updateVertex(update: Vertex) = new Graph(vertices.updated(update.value, update), this.edges)

  def removeVertex(remove: Vertex) = new Graph(vertices - remove.value, this.edges)

  def reduceEdge(reducedEdge: Edge, absorbedVertex: Vertex, mergedVertex: Vertex) = new Graph(this.vertices, edges.map {
    case (`absorbedVertex`, any) => new Edge(mergedVertex, any)
    case (any, `absorbedVertex`) => new Edge(any, mergedVertex)
    case (v1, v2) => new Edge(v1, v2)
  }.filter(edge => (edge != reducedEdge) && (edge._1 != edge._2))
  )

  override def toString: String = s"Vertices: ${this.vertices} \nEdges: ${this.edges}"
}
