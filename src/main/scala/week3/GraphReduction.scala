package week3

import week3.model._

/**
  * Created by Bertrand on 22/09/2016.
  */
object GraphReduction {

  def reduce(graph: Graph): Graph = {
    val reducedEdge = graph.randomEdge()
    val absorbedVertex = graph.getVertex(reducedEdge._2.value)
    val mergedVertex = reducedEdge._1.absorb(absorbedVertex)

    graph.reduceEdge(reducedEdge, absorbedVertex, mergedVertex)
      .updateVertex(mergedVertex)
      .removeVertex(absorbedVertex)
  }

  def reduceToCut(graph: Graph): Graph = {
    if (graph.size <= 2) {
      graph
    } else {
      reduceToCut(reduce(graph))
    }
  }

}
