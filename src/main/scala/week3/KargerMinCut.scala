package week3

import benchmark.Performance.timeIt
import week3.model._

import scala.collection.immutable.HashMap
import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Created by Bertrand on 23/09/2016.
  */
object KargerMinCut {

  val assignmentFile = "src/resources/week3/week3_kargerMinCut.txt"

  def main(args: Array[String]): Unit = {
    val fileName = if (args.length > 0) Option(args(0)).getOrElse(assignmentFile) else assignmentFile
    val lines = Source.fromFile(fileName).getLines().toList
    val minCut = timeIt(findMinCut(buildGraphFromLines(lines)))
    println(s"MinCut size = ${minCut._2}\n${minCut._1}")
  }

  def buildGraphFromLines(lines: List[String], splitKey: String = "\t"): Graph = {
    var vertices = new HashMap[String, Vertex]
    var edges = new ListBuffer[Edge]
    lines.map(l => {
      val line = l.split(splitKey)
      val vertex = new Vertex(line(0))
      vertices = vertices + (vertex.value -> vertex)
      line.drop(1).map(v => edges += new Edge(vertex, new Vertex(v)))
    })
    new Graph(vertices, edges.toList)
  }

  def findMinCut(graph: Graph): Int = {
    var minCut = graph.nbEdges
    for (i <- Iterator.range(1, 2 * graph.size)) {
      val reduced = GraphReduction.reduceToCut(graph)
      val cutSize = reduced.nbEdges / 2 // remove duplicates, this is an unoriented graph
      if (minCut > cutSize) minCut = cutSize
    }
    minCut
  }
}
