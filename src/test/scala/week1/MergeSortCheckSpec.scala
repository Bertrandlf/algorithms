package week1

import org.scalacheck.Prop.forAll
import org.scalacheck.Properties

/**
  * Created by Bertrand on 10/09/2016.
  */

object MergeSortCheckSpec extends Properties("MergeSort") {

  property("sorts a given list") = forAll { (a: List[Int]) =>
    MergeSort.mergeSort(a) == a.sorted
  }

}
