package week1

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Bertrand on 10/09/2016.
  */
class MergeSortSpec extends FlatSpec with Matchers {

  "Two lists of same length" should "get properly merged and sorted by mergeSort" in {
    val l1 = List(0, 1, 2)
    val l2 = List(2, 2, 3)
    MergeSort.mergeAndSort(l1, l2) shouldEqual List(0, 1, 2, 2, 2, 3)
  }

  "Two lists of different length" should "get merged and sorted by mergeSort" in {
    val l1 = List(0, 1, 2, 6)
    val l2 = List(2, 2, 3)
    MergeSort.mergeAndSort(l1, l2) shouldEqual List(0, 1, 2, 2, 2, 3, 6)
  }

  "A list" should "get sorted by mergeSort" in {
    val list = List(0, 2, 1, 6, 4, 2)
    MergeSort.mergeSort(list) shouldEqual list.sorted
  }

}
