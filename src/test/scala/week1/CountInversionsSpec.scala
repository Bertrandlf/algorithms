package week1

import org.scalatest.{FlatSpec, Matchers}
import week1.model.ListInversions

/**
  * Created by Bertrand on 10/09/2016.
  */
class CountInversionsSpec extends FlatSpec with Matchers {

  "Inversions count for two lists of same length" should "just work" in {
    val l1 = new ListInversions(List(1, 3, 5), 0)
    val l2 = new ListInversions(List(2, 4, 6), 0)
    CountInversions.mergeSortAndCountInversions(l1, l2) shouldEqual new ListInversions(List(1, 2, 3, 4, 5, 6), 3)
  }

  "Inversions with duplicates" should "just work" in {
    val l1 = new ListInversions(List(0, 1, 2), 1)
    val l2 = new ListInversions(List(2, 4, 6), 3)
    CountInversions.mergeSortAndCountInversions(l1, l2) shouldEqual new ListInversions(List(0, 1, 2, 2, 4, 6), 4)
  }

  "Two lists of different length with inversions" should "get merged, sorted and counted by CountInversions" in {
    val l1 = new ListInversions(List(0, 1, 2, 6), 2)
    val l2 = new ListInversions(List(3, 4, 5), 3)
    CountInversions.mergeSortAndCountInversions(l1, l2) shouldEqual new ListInversions(List(0, 1, 2, 3, 4, 5, 6), 8)
  }

  "A list" should "reveal its inversions" in {
    val list = List(0, 2, 1, 6, 4, 2)
    CountInversions.countInversions(list) shouldEqual new ListInversions(list.sorted, 4)
  }

  "A bigger list" should "reveal its inversions" in {
    val list = List(9, 12, 3, 1, 6, 8, 2, 5, 14, 13, 11, 7, 10, 4, 0)
    CountInversions.countInversions(list) shouldEqual new ListInversions(list.sorted, 56)
  }

  "An even bigger list" should "reveal its inversions" in {
    val list = List( 37, 7, 2, 14, 35, 47, 10, 24, 44, 17, 34, 11, 16, 48, 1, 39, 6, 33, 43, 26, 40, 4, 28, 5, 38, 41, 42, 12, 13, 21, 29, 18, 3, 19, 0, 32, 46, 27, 31, 25, 15, 36, 20, 8, 9, 49, 22, 23, 30, 45)
    CountInversions.countInversions(list) shouldEqual new ListInversions(list.sorted, 590)
  }

  "A massive list" should "reveal its inversions" in {
    val list = List(4, 80, 70, 23, 9, 60, 68, 27, 66, 78, 12, 40, 52, 53, 44, 8, 49, 28, 18, 46, 21, 39, 51, 7, 87, 99, 69, 62, 84, 6, 79, 67, 14, 98, 83, 0, 96, 5, 82, 10, 26, 48, 3, 2, 15, 92, 11, 55, 63, 97, 43, 45, 81, 42, 95, 20, 25, 74, 24, 72, 91, 35, 86, 19, 75, 58, 71, 47, 76, 59, 64, 93, 17, 50, 56, 94, 90, 89, 32, 37, 34, 65, 1, 73, 41, 36, 57, 77, 30, 22, 13, 29, 38, 16, 88, 61, 31, 85, 33, 54)
    CountInversions.countInversions(list) shouldEqual new ListInversions(list.sorted, 2372)
  }

}
