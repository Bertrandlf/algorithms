package week2

import org.scalatest.{FlatSpec, Matchers}
import week2.model.QuickSortList

/**
  * Created by Bertrand on 14/09/2016.
  */
class QuickSortSpec extends FlatSpec with Matchers {

  "A list" should "get partitioned" in {
    val l = new QuickSortList(Array(3, 2, 1, 7, 6, 5, 4, 8))
    QuickSort.partitionFirst(l, 0, l.length - 1)
    l.list shouldEqual Array(1, 2, 3, 7, 6, 5, 4, 8)
  }

  "A list" should "get sorted" in {
    val l = new QuickSortList(Array(3, 2, 1, 7, 6, 5, 4, 8))
    QuickSort.quickSort(l, 0, l.length - 1)
    l.list shouldEqual Array(1, 2, 3, 4, 5, 6, 7, 8)
  }

  "Best of three" should "return the median" in {
    QuickSort.getMedianIndex((2, 3), (5, 10), (6, 3)) shouldEqual 10
  }

}
