package week2

import org.scalacheck.Prop._
import org.scalacheck.Properties

/**
  * Created by Bertrand on 14/09/2016.
  */
object FunctionalQuickSortSpec extends Properties("FunctionalQuickSort") {

  property("sorts a given list") = forAll { (a: Array[Int]) =>
    FunctionalQuickSort.quickSort(a).toList == a.sorted.toList
  }

}
