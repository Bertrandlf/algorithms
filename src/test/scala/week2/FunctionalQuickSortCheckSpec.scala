package week2

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Bertrand on 14/09/2016.
  */
class FunctionalQuickSortCheckSpec extends FlatSpec with Matchers {

  "An array" should "get sorted" in {
    val l = Array(3, 2, 1, 7, 6, 5, 4, 8)
    FunctionalQuickSort.quickSort(l) shouldEqual Array(1, 2, 3, 4, 5, 6, 7, 8)
  }

}
