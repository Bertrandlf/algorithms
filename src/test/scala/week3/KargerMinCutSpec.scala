package week3

import org.scalatest.{FlatSpec, Matchers}
import KargerMinCut.{buildGraphFromLines, findMinCut}

import scala.io.Source
/**
  * Created by Bertrand on 25/09/2016.
  */
class KargerMinCutSpec extends FlatSpec with Matchers {

  val smallTest = "src/resources/week3/week3_small_test.txt"
  val bigTest = "src/resources/week3/week3_big_test.txt"

  "Solver on small test file graph" should "find a cut of 1" in {
    findMinCut(buildGraphFromLines(Source.fromFile(smallTest).getLines().toList, " ")) shouldEqual 1
  }
  "Solver on big test file graph" should "find a cut of 3" in {
    findMinCut(buildGraphFromLines(Source.fromFile(bigTest).getLines().toList, " ")) shouldEqual 3
  }
}
