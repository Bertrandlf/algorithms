package week6

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Bertrand on 14/10/2016.
  */
class MedianHeapSpec extends FlatSpec with Matchers {

  it should "maintain median for one element" in {
    val medianHeap = new MedianHeap()
    medianHeap.enqueue(1)
    medianHeap.median shouldEqual 1
  }

  it should "maintain median for 10 element" in {
    val medianHeap = new MedianHeap()
    (1 to 10).map(value => medianHeap.enqueue(value))
    medianHeap.median shouldEqual 5
  }

}
