package week4.model

import org.scalatest.{FlatSpec, Matchers}

/**
  * Created by Bertrand on 2/10/2016.
  */
class DirectedGraphSpec extends FlatSpec with Matchers {

  val expectedGraph = DirectedGraph(4)
  List((0, 1), (3, 0), (1, 2)).foreach(edge => expectedGraph.addEdge(edge._1, edge._2))

  "Reading test_small_graph" should "yield the expected graph" in {
    DirectedGraph.fromFile("src/resources/week4/week4_test_small_graph.txt") shouldEqual expectedGraph
  }
}
